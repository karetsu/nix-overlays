{
  # font-0 = "mononoki:pixelsize=12;3";
  font-0 = "Iosevka Custom:pixelsize=12;3";
  font-1 = "Iosevka Nerd Font:pixelsize=12;3";
  font-2 = "Iosevka Custom:pixelsize=12:style=bold;3";
  font-3 = "Iosevka Custom:pixelsize=12:style=hair;3";
  font-4 = "Overpass:pixelsize=12:style=regular;2";
  font-5 = "Overpass:pixelsize=64:style=regular;22";

  xfonts = "xft:Iosevka Custom:pixelsize=14,"
    + "xft:Iosevka Nerd Font:style=Regular:pixelsize=14,"
    + "xft:Material Icons:pixelsize=14," + "xft:Source Code Pro:pixelsize=14,"
    + "xft:Overpass:style=Regular:pixelsize=12";
}
